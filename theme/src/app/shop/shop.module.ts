import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ShopRoutingModule} from './shop-routing.module';
import {SharedModule} from '../shared/shared.module';
import {SlickCarouselModule} from 'ngx-slick-carousel';
import {BarRatingModule} from 'ngx-bar-rating';
import {RangeSliderModule} from 'ngx-rangeslider-component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {NgxPayPalModule} from 'ngx-paypal';
import {NgxImgZoomModule} from 'ngx-img-zoom';
// Home-two components
import {HomeTwoComponent} from './home-two/home-two.component';
import {SliderTwoComponent} from './home-two/slider/slider.component';
import {ProductSliderTwoComponent} from './home-two/product-slider/product-slider.component';
import {ParallaxBannerTwoComponent} from './home-two/parallax-banner/parallax-banner.component';
import {ProductTabTwoComponent} from './home-two/product-tab/product-tab.component';
// Products Components
import {ProductComponent} from './product/product.component';
import {ProductBoxComponent} from './product/product-box/product-box.component';
import {ProductBoxVerticalComponent} from './product/product-box-vertical/product-box-vertical.component';
import {CollectionRightSidebarComponent} from './product/collection/collection-right-sidebar/collection-right-sidebar.component';
import {ColorComponent} from './product/collection/filter/color/color.component';
import {PriceComponent} from './product/collection/filter/price/price.component';
import {CategoriesComponent} from './product/widgets/categories/categories.component';
import {NewProductComponent} from './product/widgets/new-product/new-product.component';
import {SearchComponent} from './product/search/search.component';
import {ProductCompareComponent} from './product/product-compare/product-compare.component';
import {WishlistComponent} from './product/wishlist/wishlist.component';
import {CartComponent} from './product/cart/cart.component';
import {CheckoutComponent} from './product/checkout/checkout.component';
import {SuccessComponent} from './product/success/success.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ShopRoutingModule,
    SharedModule,
    SlickCarouselModule,
    BarRatingModule,
    RangeSliderModule,
    InfiniteScrollModule,
    NgxPayPalModule,
    NgxImgZoomModule
  ],
  declarations: [
    // Home two
    HomeTwoComponent,
    SliderTwoComponent,
    ProductSliderTwoComponent,
    ParallaxBannerTwoComponent,
    ProductTabTwoComponent,
    // Product
    ProductComponent,
    ProductBoxComponent,
    ProductBoxVerticalComponent,
    CollectionRightSidebarComponent,
    ColorComponent,
    PriceComponent,
    CategoriesComponent,
    NewProductComponent,
    SearchComponent,
    ProductCompareComponent,
    WishlistComponent,
    CartComponent,
    CheckoutComponent,
    SuccessComponent
  ]
})
export class ShopModule {
}
