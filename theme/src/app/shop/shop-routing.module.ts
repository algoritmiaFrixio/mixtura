import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {HomeTwoComponent} from './home-two/home-two.component';
import {CollectionRightSidebarComponent} from './product/collection/collection-right-sidebar/collection-right-sidebar.component';
import {SearchComponent} from './product/search/search.component';
import {WishlistComponent} from './product/wishlist/wishlist.component';
import {ProductCompareComponent} from './product/product-compare/product-compare.component';
import {CartComponent} from './product/cart/cart.component';
import {CheckoutComponent} from './product/checkout/checkout.component';
import {SuccessComponent} from './product/success/success.component';

// Routes
const routes: Routes = [
  {
    path: '',
    component: HomeTwoComponent
  },
  {
    path: 'categoria/:category',
    component: CollectionRightSidebarComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'wishlist',
    component: WishlistComponent
  },
  {
    path: 'compare',
    component: ProductCompareComponent
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'checkout',
    component: CheckoutComponent
  },
  {
    path: 'checkout/success',
    component: SuccessComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ShopRoutingModule {
}
