// Product Colors
export type ProductColor = 'white' | 'black' | 'red' | 'green' | 'purple' | 'yellow' | 'blue' | 'gray' | 'orange' | 'pink';

// Product Size
export type ProductSize = 'M' | 'L' | 'XL';

// Product
export interface Product {
  categoria?: { id?: number; nombre?: string };
  id?: number;
  categoria_id?: number;
  coleccion?: { id?: number; nombre?: string };
  coleccion_id?: number;
  color?: string;
  descripcion?: string;
  descuento?: number;
  detalles?: string;
  nombre?: string;
  picture?: { id?: number; src?: string; color_id?: number; producto_id?: number; color?: { id?: number; color?: ProductColor } }[];
  precio?: { id?: number; precio_id?: number; producto_id?: number; precio?: number; tipo?: { id?: number; tipo_moneda?: string; } }[];
  referencia?: string;
  stock?: number;
  pictures?: string[];
  tela?: string;
  colors?: ProductColor[];
  size?: { id?: number; talla_id?: number; producto_id?: number; talla?: { id?: number; nombre?: ProductSize; } }[];
  sizes?: ProductSize[];
  variants?: any[];
  price?: number;
  new?: boolean;
  sale?: boolean;
  salePrice?: number;
//   id?: number;
//   name?: string;
//   salePrice?: number;
//   discount?: number;
//   shortDetails?: string;
//   description?: string;
//   stock?: number;
//   new?: boolean;
//   sale?: boolean;
//   category?: string;
//   tags?: ProductSize[];
}

//
// // Color Filter
export interface ColorFilter {
  color?: ProductColor;
}



