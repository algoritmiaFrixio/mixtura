import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
// Services
import {WINDOW_PROVIDERS} from './services/windows.service';
import {InstagramService} from './services/instagram.service';
import {ProductsService} from './services/products.service';
import {WishlistService} from './services/wishlist.service';
import {CartService} from './services/cart.service';
import {OrderService} from './services/order.service';
import {PaginationService} from './classes/paginate';
// Pipes
import {OrderByPipe} from './pipes/order-by.pipe';
// components
import {HeaderThreeComponent} from './header/header-three/header-three.component';
import {TopbarComponent} from './header/widgets/topbar/topbar.component';
import {NavbarComponent} from './header/widgets/navbar/navbar.component';
import {SettingsComponent} from './header/widgets/settings/settings.component';
import {FooterTwoComponent} from './footer/footer-two/footer-two.component';
import {InformationComponent} from './footer/widgets/information/information.component';
import {CategoriesComponent} from './footer/widgets/categories/categories.component';
import {WhyWeChooseComponent} from './footer/widgets/why-we-choose/why-we-choose.component';
import {ContactComponent} from './footer/widgets/contact/contact.component';
import {CopyrightComponent} from './footer/widgets/copyright/copyright.component';
import {SocialComponent} from './footer/widgets/social/social.component';

@NgModule({
  exports: [
    CommonModule,
    TranslateModule,
    HeaderThreeComponent,
    FooterTwoComponent,
    OrderByPipe
  ],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule
  ],
  declarations: [
    HeaderThreeComponent,
    FooterTwoComponent,
    OrderByPipe,
    NavbarComponent,
    SettingsComponent,
    TopbarComponent,
    InformationComponent,
    CategoriesComponent,
    WhyWeChooseComponent,
    ContactComponent,
    CopyrightComponent,
    SocialComponent
  ],
  providers: [
    WINDOW_PROVIDERS,
    InstagramService,
    ProductsService,
    WishlistService,
    CartService,
    OrderService,
    PaginationService
  ]
})
export class SharedModule {
}
