import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Product} from '../classes/product';
import {BehaviorSubject, Observable} from 'rxjs';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable()

export class ProductsService {

  public currency = 'USD';
  public compareProducts: BehaviorSubject<Product[]> = new BehaviorSubject([]);
  public compareProduct: Product[] = [];

  // Initialize
  constructor(private http: HttpClient, private toastrService: ToastrService) {
    this.compareProducts.subscribe(products => this.compareProduct = products);
  }

  // Observable Product Array
  private products(tipo = 'all'): Observable<string[]> {
    return this.http.get(`${environment.urlServidor}productos/${tipo}`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    })
      .pipe(map((response: HttpResponse<string[]>) => response.body));
  }

  // Get Products
  public getProducts(): Observable<string[]> {
    return this.products();
  }


  // Get Products By category
  public getProductByCategory(category: string): Observable<string[]> {
    return this.products(category.toLowerCase());
  }

  // Get Products
  public getComapreProducts(): Observable<Product[]> {
    return this.compareProducts.asObservable();
  }

  // If item is aleready added In compare
  public hasProduct(product: Product): boolean {
    const item = this.compareProduct.find(item => item.id === product.id);
    return item !== undefined;
  }

  // Add to compare
  public addToCompare(product: Product): Product | boolean {
    let item: Product | boolean = false;
    if (this.hasProduct(product)) {
      item = this.compareProduct.filter(item => item.id === product.id)[0];
      const index = this.compareProduct.indexOf(item);
    } else {
      if (this.compareProduct.length < 4) {
        this.compareProduct.push(product);
      } else {
        this.toastrService.warning('Maximum 4 products are in compare.');
      } // toasr services
    }
    return item;
  }

  // Removed Product
  public removeFromCompare(product: Product) {
    if (product === undefined) {
      return;
    }
    const index = this.compareProduct.indexOf(product);
    this.compareProduct.splice(index, 1);
  }

}
