import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AboutUsComponent} from './about-us/about-us.component';
import {ErrorPageComponent} from './error-page/error-page.component';
import {LookbookComponent} from './lookbook/lookbook.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {CollectionComponent} from './collection/collection.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {ContactComponent} from './contact/contact.component';
import {CompareComponent} from './compare/compare.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FaqComponent} from './faq/faq.component';


const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'about-us',
        component: AboutUsComponent
      },
      {
        path: '404',
        component: ErrorPageComponent
      },
      {
        path: 'lookbook',
        component: LookbookComponent
      },
      {
        path: 'login',
        component: LoginComponent
      },
      {
        path: 'register',
        component: RegisterComponent
      },
      {
        path: 'collection',
        component: CollectionComponent
      },
      {
        path: 'forgetpassword',
        component: ForgetPasswordComponent
      },
      {
        path: 'contact',
        component: ContactComponent
      },
      {
        path: 'compare',
        component: CompareComponent
      },
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'faq',
        component: FaqComponent
      }
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
