import {Routes} from '@angular/router';

import {MainComponent} from './main/main.component';

export const rootRouterConfig: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'home',
        loadChildren: './shop/shop.module#ShopModule'
      },
      {
        path: 'pages',
        loadChildren: './pages/pages.module#PagesModule'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];

