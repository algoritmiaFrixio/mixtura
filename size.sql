--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2019-02-20 17:41:40

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 215 (class 1259 OID 18344)
-- Name: product_sizes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_sizes (
    id integer NOT NULL,
    talla_id integer NOT NULL,
    producto_id integer NOT NULL
);


ALTER TABLE public.product_sizes OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 18342)
-- Name: product_sizes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_sizes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_sizes_id_seq OWNER TO postgres;

--
-- TOC entry 2887 (class 0 OID 0)
-- Dependencies: 214
-- Name: product_sizes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_sizes_id_seq OWNED BY public.product_sizes.id;


--
-- TOC entry 2753 (class 2604 OID 18425)
-- Name: product_sizes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sizes ALTER COLUMN id SET DEFAULT nextval('public.product_sizes_id_seq'::regclass);


--
-- TOC entry 2881 (class 0 OID 18344)
-- Dependencies: 215
-- Data for Name: product_sizes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_sizes (id, talla_id, producto_id) FROM stdin;
1	3	3
2	4	3
3	5	3
4	3	4
5	4	4
6	5	4
7	3	5
8	4	5
9	5	5
10	3	6
11	4	6
12	5	6
13	3	7
14	4	7
15	5	7
16	3	8
17	4	8
18	5	8
\.


--
-- TOC entry 2888 (class 0 OID 0)
-- Dependencies: 214
-- Name: product_sizes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_sizes_id_seq', 18, true);


--
-- TOC entry 2755 (class 2606 OID 18349)
-- Name: product_sizes product_sizes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sizes
    ADD CONSTRAINT product_sizes_pkey PRIMARY KEY (id);


--
-- TOC entry 2758 (class 2606 OID 18426)
-- Name: product_sizes product_sizes_id_product_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sizes
    ADD CONSTRAINT product_sizes_id_product_foreign FOREIGN KEY (producto_id) REFERENCES public.products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2757 (class 2606 OID 18355)
-- Name: product_sizes product_sizes_producto_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sizes
    ADD CONSTRAINT product_sizes_producto_id_foreign FOREIGN KEY (producto_id) REFERENCES public.products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2756 (class 2606 OID 18350)
-- Name: product_sizes product_sizes_talla_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_sizes
    ADD CONSTRAINT product_sizes_talla_id_foreign FOREIGN KEY (talla_id) REFERENCES public.sizes(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2019-02-20 17:41:40

--
-- PostgreSQL database dump complete
--

