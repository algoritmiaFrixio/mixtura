<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picture extends Model
{
    protected $hidden = ['updated_at', 'created_at'];

    public function color()
    {
        return $this->belongsTo(Color::class, 'color_id', 'id');
    }
}
