<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $hidden = ['created_at', 'updated_at'];
    protected $fillable = ['nombre', 'descuento', 'detalles', 'color', 'referencia', 'tela', 'descripcion', 'stock', 'categoria_id', 'coleccion_id'];

    public function categoria()
    {
        return $this->belongsTo(Category::class, 'categoria_id', 'id');
    }

    public function coleccion()
    {
        return $this->belongsTo(Collection::class, 'coleccion_id', 'id');
    }

    public function picture()
    {
        return $this->hasMany(Picture::class, 'producto_id', 'id');
    }

    public function precio()
    {
        return $this->hasMany(ProductPrice::class, 'producto_id', 'id');
    }

    public function size()
    {
        return $this->hasMany(ProductSize::class, 'producto_id', 'id');
    }

}

