<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    protected $hidden = ['updated_at', 'created_at'];
}
