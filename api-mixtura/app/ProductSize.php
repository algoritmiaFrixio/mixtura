<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSize extends Model
{

    public function talla()
    {
        return $this->belongsTo(Size::class, 'talla_id', 'id');
    }
}
