<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    protected $hidden = ['updated_at', 'created_at'];

    public function tipo()
    {
        return $this->belongsTo(Price::class, 'precio_id', 'id');
    }
}
