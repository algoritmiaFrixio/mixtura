<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('hola');
        // Se comprueba que realmente sea la ruta de un directorio
        if (is_dir('../images/products')) {
            // Abre un gestor de directorios para la ruta indicada
            $gestor = opendir('../images/products');

            // Recorre todos los archivos del directorio
            while (($archivo = readdir($gestor)) !== false) {
                // Solo buscamos archivos sin entrar en subdirectorios
                if (is_file('../images/products' . "/" . $archivo)) {
                    echo "<img src='" . '../images/products' . "/" . $archivo . "' width='200px' alt='" . $archivo . "' title='" . $archivo . "'>";
                }
            }

            // Cierra el gestor de directorios
            closedir($gestor);
        } else {
            echo "No es una ruta de directorio valida<br/>";
        }
//
//        $products = Product::with(['categoria', 'coleccion', 'picture', 'precio', 'size'])->get();
//        foreach ($products as $product) {
//            foreach ($product->picture as $item) {
//                $item->color;
//            }
//            foreach ($product->precio as $item) {
//                $item->tipo;
//            }
//            foreach ($product->size as $item) {
//                $item->talla;
//            }
//        }
//        return response()->json($products, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
    }
}
