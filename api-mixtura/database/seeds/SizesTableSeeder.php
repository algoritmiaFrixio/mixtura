<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sizes')->insert([
            'nombre' => '4'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '6'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '8'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '10'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '12'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '14'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '16'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '18'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '20'
        ]);
        DB::table('sizes')->insert([
            'nombre' => 'S'
        ]);
        DB::table('sizes')->insert([
            'nombre' => 'M'
        ]);
        DB::table('sizes')->insert([
            'nombre' => 'L'
        ]);
        DB::table('sizes')->insert([
            'nombre' => 'XL'
        ]);
        DB::table('sizes')->insert([
            'nombre' => 'XXL'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '3XL'
        ]);
        DB::table('sizes')->insert([
            'nombre' => '4XL'
        ]);
    }
}
