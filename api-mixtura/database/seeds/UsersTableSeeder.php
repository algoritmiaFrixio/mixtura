<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'usuario' => 'mixtura',
            'contrasena' => Hash::make('1a2s3d4f5g'),
            'token' => str_random(60)
        ]);
    }
}
