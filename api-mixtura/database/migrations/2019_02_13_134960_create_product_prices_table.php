<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_prices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('precio_id');
            $table->foreign('precio_id')
                ->references('id')
                ->on('prices')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->unsignedInteger('producto_id');
            $table->foreign('producto_id')
                ->references('id')
                ->on('products')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->float('precio');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_prices');
    }
}
