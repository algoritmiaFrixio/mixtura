<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('productos');
            $table->string('cantidad');
            $table->string('tallas');
            $table->string('identificacion');
            $table->enum('estado', ['En proceso', 'Confirmado', 'En camino', 'Entregado'])->default('En proceso');
            $table->enum('estado_compra', ['Aprobado', 'Rechazado', 'En espera'])->nullable();
            $table->string('metodo', 50)->nullable();
            $table->string('entidad', 50)->nullable();
            $table->unsignedInteger('cliente_id')->nullable();
            $table->foreign('cliente_id')
                ->references('id')
                ->on('clients')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->unsignedInteger('pago_id');
            $table->foreign('pago_id')
                ->references('id')
                ->on('pagos')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
