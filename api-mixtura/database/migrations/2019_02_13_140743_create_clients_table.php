<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('identificacion', 50);
            $table->string('correo', 100)->unique()->nullable();
            $table->string('contrasena', 60);
            $table->string('nombre_completo', 100);
            $table->string('telefono', 25)->nullable();
            $table->string('ciudad', 50);
            $table->string('departamento', 50);
            $table->string('pais', 50);
            $table->string('direccion', 100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
