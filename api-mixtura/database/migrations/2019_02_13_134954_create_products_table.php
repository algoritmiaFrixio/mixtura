<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->float('descuento')->unsigned()->default(0);
            $table->string('detalles', 25)->unique();
            $table->string('color', 200)->nullable();
            $table->string('referencia')->nullable();
            $table->string('tela', 100);
            $table->text('descripcion');
            $table->integer('stock')->unsigned();
            $table->unsignedInteger('categoria_id');
            $table->foreign('categoria_id')
                ->references('id')
                ->on('categories')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->unsignedInteger('coleccion_id');
            $table->foreign('coleccion_id')
                ->references('id')
                ->on('collections')
                ->onUpdate('restrict')
                ->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
