<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 50);
            $table->string('total', 50);
            $table->string('impuestos', 50);
            $table->string('total_sin_iva', 50);
            $table->string('moneda', 50);
            $table->string('prueba', 50)->default('1');
            $table->string('correo', 50);
            $table->string('nombre_completo', 50);
            $table->string('direccion', 50);
            $table->string('ciudad', 50);
            $table->string('pais', 50);
            $table->string('envio', 50);
            $table->string('telefono', 50);
            $table->string('departamento', 50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}
