--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2019-02-20 17:41:12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 211 (class 1259 OID 18308)
-- Name: product_prices; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.product_prices (
    id integer NOT NULL,
    precio_id integer NOT NULL,
    producto_id integer NOT NULL,
    precio double precision NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.product_prices OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 18306)
-- Name: product_prices_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.product_prices_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_prices_id_seq OWNER TO postgres;

--
-- TOC entry 2886 (class 0 OID 0)
-- Dependencies: 210
-- Name: product_prices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.product_prices_id_seq OWNED BY public.product_prices.id;


--
-- TOC entry 2753 (class 2604 OID 18423)
-- Name: product_prices id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_prices ALTER COLUMN id SET DEFAULT nextval('public.product_prices_id_seq'::regclass);


--
-- TOC entry 2880 (class 0 OID 18308)
-- Dependencies: 211
-- Data for Name: product_prices; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.product_prices (id, precio_id, producto_id, precio, created_at, updated_at) FROM stdin;
1	1	3	0	\N	\N
2	1	4	0	\N	\N
3	1	5	0	\N	\N
4	1	6	0	\N	\N
5	1	7	0	\N	\N
6	1	8	0	\N	\N
\.


--
-- TOC entry 2887 (class 0 OID 0)
-- Dependencies: 210
-- Name: product_prices_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.product_prices_id_seq', 6, true);


--
-- TOC entry 2755 (class 2606 OID 18313)
-- Name: product_prices product_prices_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_prices
    ADD CONSTRAINT product_prices_pkey PRIMARY KEY (id);


--
-- TOC entry 2756 (class 2606 OID 18314)
-- Name: product_prices product_prices_precio_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_prices
    ADD CONSTRAINT product_prices_precio_id_foreign FOREIGN KEY (precio_id) REFERENCES public.prices(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2757 (class 2606 OID 18319)
-- Name: product_prices product_prices_producto_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.product_prices
    ADD CONSTRAINT product_prices_producto_id_foreign FOREIGN KEY (producto_id) REFERENCES public.products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2019-02-20 17:41:12

--
-- PostgreSQL database dump complete
--

