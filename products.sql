--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2019-02-20 17:40:19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 209 (class 1259 OID 18284)
-- Name: products; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.products (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    descuento double precision DEFAULT '0'::double precision NOT NULL,
    detalles character varying(25) NOT NULL,
    color character varying(200),
    referencia character varying(255),
    tela character varying(100) NOT NULL,
    descripcion text NOT NULL,
    stock integer NOT NULL,
    categoria_id integer NOT NULL,
    coleccion_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.products OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 18282)
-- Name: products_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.products_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.products_id_seq OWNER TO postgres;

--
-- TOC entry 2889 (class 0 OID 0)
-- Dependencies: 208
-- Name: products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.products_id_seq OWNED BY public.products.id;


--
-- TOC entry 2754 (class 2604 OID 18422)
-- Name: products id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products ALTER COLUMN id SET DEFAULT nextval('public.products_id_seq'::regclass);


--
-- TOC entry 2883 (class 0 OID 18284)
-- Dependencies: 209
-- Data for Name: products; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.products (id, nombre, descuento, detalles, color, referencia, tela, descripcion, stock, categoria_id, coleccion_id, created_at, updated_at) FROM stdin;
3	vestido flores azul	0	n/a1	Azul	n/a	n/a	n/a	5	2	1	\N	\N
5	Vestido flores azul	0	n/a3	Azul	n/a	n/a	n/a	5	2	1	\N	\N
6	Blusa rosada	0	n/a4	Rosado	n/a	n/a	n/a	5	1	1	\N	\N
8	Blusa azul	0	n/a6	Azul	n/a	n/a	n/a	5	1	1	\N	\N
4	vestido a rayas de colores	0	n/a2	Coral	n/a	n/a	n/a	5	2	1	\N	\N
7	Blusa figuras de colores	0	n/a5	Rosado	n/a	n/a	n/a	5	1	1	\N	\N
\.


--
-- TOC entry 2890 (class 0 OID 0)
-- Dependencies: 208
-- Name: products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.products_id_seq', 8, true);


--
-- TOC entry 2756 (class 2606 OID 18305)
-- Name: products products_detalles_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_detalles_unique UNIQUE (detalles);


--
-- TOC entry 2758 (class 2606 OID 18293)
-- Name: products products_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_pkey PRIMARY KEY (id);


--
-- TOC entry 2759 (class 2606 OID 18294)
-- Name: products products_categoria_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_categoria_id_foreign FOREIGN KEY (categoria_id) REFERENCES public.categories(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2760 (class 2606 OID 18299)
-- Name: products products_coleccion_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.products
    ADD CONSTRAINT products_coleccion_id_foreign FOREIGN KEY (coleccion_id) REFERENCES public.collections(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2019-02-20 17:40:21

--
-- PostgreSQL database dump complete
--

