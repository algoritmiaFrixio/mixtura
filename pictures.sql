--
-- PostgreSQL database dump
--

-- Dumped from database version 11.1
-- Dumped by pg_dump version 11.1

-- Started on 2019-02-20 17:42:08

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 213 (class 1259 OID 18326)
-- Name: pictures; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pictures (
    id integer NOT NULL,
    src character varying(255) NOT NULL,
    color_id integer,
    producto_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.pictures OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 18324)
-- Name: pictures_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pictures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pictures_id_seq OWNER TO postgres;

--
-- TOC entry 2886 (class 0 OID 0)
-- Dependencies: 212
-- Name: pictures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pictures_id_seq OWNED BY public.pictures.id;


--
-- TOC entry 2753 (class 2604 OID 18424)
-- Name: pictures id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures ALTER COLUMN id SET DEFAULT nextval('public.pictures_id_seq'::regclass);


--
-- TOC entry 2880 (class 0 OID 18326)
-- Dependencies: 213
-- Data for Name: pictures; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pictures (id, src, color_id, producto_id, created_at, updated_at) FROM stdin;
1	3/Capture00367.jpg	6	3	\N	\N
3	5/Capture00384.jpg	6	5	\N	\N
6	8/Capture00417.jpg	6	8	\N	\N
4	6/Capture00390.jpg	10	6	\N	\N
2	4/Capture00376.jpg	3	4	\N	\N
5	7/Capture00401.jpg	10	7	\N	\N
\.


--
-- TOC entry 2887 (class 0 OID 0)
-- Dependencies: 212
-- Name: pictures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pictures_id_seq', 6, true);


--
-- TOC entry 2755 (class 2606 OID 18331)
-- Name: pictures pictures_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures_pkey PRIMARY KEY (id);


--
-- TOC entry 2756 (class 2606 OID 18332)
-- Name: pictures pictures_color_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures_color_id_foreign FOREIGN KEY (color_id) REFERENCES public.colors(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 2757 (class 2606 OID 18337)
-- Name: pictures pictures_producto_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pictures
    ADD CONSTRAINT pictures_producto_id_foreign FOREIGN KEY (producto_id) REFERENCES public.products(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


-- Completed on 2019-02-20 17:42:09

--
-- PostgreSQL database dump complete
--

